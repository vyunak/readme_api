
`Init atm` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/initialization-cash | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`

```
request: {"uid": "cash_terminal_ilya"}

**RESPONSE**
{
    "status": "success"
}
```

`Checking the phone number for a transaction` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/check-phone-number | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  phone_number | | `+380000000000`

```
request: {"uid": "cash_terminal_ilya", "phone_number": "+380000000000"}

**RESPONSE**
{
    "status": "success"
}
```

`Create object transaction` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/create-object-transaction | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  amount | $ | `100/200/300` or `null` for gold `WIP`
| |  phone_number | | `+380000000000`

```
request: {"uid": "cash_terminal_ilya", "phone_number": "+380000000000", "amount": 100}

**RESPONSE**
{
    "status": "success",
    "amount_crypto": "1.59344725",
    "currency": "1GOLD",
    "timeout": 7200,
    "wallet": "0x976ee6bbcf6fdd815b685c411d1fbd0465d79ea7",
    "qrcode_url": "https://office.golden.com/scan-qr?id=CPEJ7HUFA9LK72UODOTPKJABX4",
    "transaction_id": "CPEJ7HUFA9LK72UODOTPKJABX4"
}
```

`Change status transaction` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/change-status-transaction | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  txid |  transaction_id | `CPEJ1EY92SPNTWEDWSS057ONNK`
| |  status | | `0 - Error give` `1 - success`

```
request: {"uid": "cash_terminal_ilya", "txid": "CPEJ7HUFA9LK72UODOTPKJABX4", "status": 1}

**RESPONSE**
{
    "status": "success"
}
```

`Check transaction for give object` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/check-transaction | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  txid |  transaction_id | `CPEJ1EY92SPNTWEDWSS057ONNK`
| |  test |  success_for_test | `1`

```
request: {"uid": "cash_terminal_ilya", "txid": "CPEJ7HUFA9LK72UODOTPKJABX4", "test": 1}

**RESPONSE**
{
    "status": "success",
    "info": {
        "text": "Waiting for buyer funds...",
        "amount_crypto": "1.59344725",
        "received_crypto": "0.00000000",
        "payment_wallet": "0x976ee6bbcf6fdd815b685c411d1fbd0465d79ea7",
        "status": 0
    }
}
```

`Create crypto transaction` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/create-сrypto-transaction | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  amount | $ | `100/200/300` or `null` for gold `WIP`
| |  wallet | | `0x247af53b629cde4460e2703b56227a0c3d65d229`

```
 request: {"uid": "cash_terminal_ilya", "wallet": "0x247af53b629cde4460e2703b56227a0c3d65d229", "amount": 100}

**RESPONSE**
{
    "status": "success",
    "system_id": 48,
    "amount": "100",
    "amount_crypto": "1.54864725",
    "wallet": "0x247af53b629cde4460e2703b56227a0c3d65d229"
}
```

`Confirm crypto transaction` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/сonfirm-сrypto-transaction | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  id | system_id - transaction | `42`

```
request: {"uid": "cash_terminal_ilya", "id": 48}

**RESPONSE**
{
    "status": "success",
    "amount": "100",
    "amount_crypto": "1.54864725",
    "transaction_id": "test",
    "wallet": "0x247af53b629cde4460e2703b56227a0c3d65d229"
}
```

`Cancel crypto transaction` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/cancel-crypto-transaction | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`
| |  id | system_id - transaction | `42`

```
request: {"uid": "cash_terminal_ilya", "id": "48"}

**RESPONSE**
{
    "status": "success"
}
```

`Get Exchange Rate` | #1 | #2 | #3 
--- | --- | --- | --- |
| Type | POST | | |
| Url | /atm/get-exchange-rate | | |
| **Response** | | | **Example** |
| request | | |
| |  uid |  unique | `cash_terminal_ilya`

```
request: {"uid": "cash_terminal_ilya"}

**RESPONSE**
{
    "status": "success",
    "info": {
        "price": "64.57248421",
        "volume_24h": 55047.86325632,
        "percent_change_1h": 0.91600015,
        "percent_change_24h": 1.80907822,
        "percent_change_7d": 1.77611585,
        "market_cap": 0,
        "last_updated": "2020-10-09T13:19:43.000Z"
    }
}
```